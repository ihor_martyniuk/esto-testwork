<?php


namespace Feature;


use App\Repositories\Transactions\CommonTransactionRepository;
use App\Services\t\TransactionEntityInterface;
use App\Services\t\TransactionManager;
use App\Transactions;
use App\User;
use Tests\TestCase;

class CreateTransactionTest extends TestCase
{

    /** @test */
    public function is_a_transaction_entity()
    {
        $user = factory(User::class)->create();
        $this->be($user);
        $entity = (new TransactionManager(new CommonTransactionRepository()))
            ->add(Transactions::TYPE_DEBIT, 100);
        $this->assertInstanceOf(TransactionEntityInterface::class, $entity);
    }
}
