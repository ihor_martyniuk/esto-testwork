<?php

namespace App\Exceptions;

use Exception;

/**
 * Class TransactionInputDataException
 * @package App\Exceptions
 */
class TransactionInitDataException extends Exception
{
    /**
     * @var string
     */
    protected string $defaultMessage = 'Data does not valid for initialization!';

    /**
     * @return string
     */
    public function render()
    {
        return $this->defaultMessage;
    }
}
