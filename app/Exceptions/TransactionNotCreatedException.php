<?php

namespace App\Exceptions;

use Exception;

class TransactionNotCreatedException extends Exception
{
    /**
     * @var string
     */
    protected string $defaultMessage = 'Transaction is not created';

    /**
     * @return string
     */
    public function render()
    {
        return $this->defaultMessage;
    }
}
