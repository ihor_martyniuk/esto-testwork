<?php


namespace App\Repositories;


/**
 * Interface RepositoryInterface
 * @package App\Repositories
 */
interface RepositoryInterface
{
    /**
     *
     * NOT full interface, depends task
     *
     */

    /**
     * @param array $data
     * @return array|null
     */
    public function create(array $data): ?array ;
}
