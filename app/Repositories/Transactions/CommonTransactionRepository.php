<?php


namespace App\Repositories\Transactions;


use App\Transactions;
use App\User;
use Illuminate\Support\Facades\DB;

/**
 * Class CommonTransactionRepository
 * @package App\Repositories\Transactions
 */
class CommonTransactionRepository implements TransactionRepositoryInterface
{
    /**
     * @param array $data
     * @return array|null
     */
    public function create(array $data): ?array
    {
        $result = null;

        $model = new Transactions();
        $model->setRawAttributes($data);

        if ($model->save()) {
            $result = $model->getAttributes();
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getLastTenUsersTransactions(): array
    {
        $query = User::query()
            ->select(['users.email as user', DB::raw('SUM(transactions.amount) as amount')])
            ->leftJoin('transactions', 'users.id', '=', 'transactions.user_id')
            ->where('transactions.type', Transactions::TYPE_DEBIT)
            ->groupBy('users.id')
            ->orderByDesc('users.id')
            ->limit(10);

        return $query->get()->toArray();
    }
}
