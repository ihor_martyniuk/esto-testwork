<?php


namespace App\Repositories\Transactions;


use App\Repositories\RepositoryInterface;

/**
 * Interface TransactionRepositoryInterface
 * @package App\Repositories
 */
interface TransactionRepositoryInterface extends RepositoryInterface
{
    /**
     * as example
     * @return array
     */
    public function getLastTenUsersTransactions(): array ;
}
