<?php


namespace App\Http\Controllers;


use App\Repositories\Transactions\CommonTransactionRepository;
use App\Services\t\TransactionManager;
use App\Transactions;
use Illuminate\Http\Request;

class TransactionsController extends Controller
{
    public function create(Request $request)
    {
        $entity = (new TransactionManager(new CommonTransactionRepository()))
            ->add(Transactions::TYPE_DEBIT, 100);
        return  response()
            ->json([
                'id' => $entity->getId()
            ]);
    }

    public function lastTen()
    {
        return  response()
            ->json((new TransactionManager(new CommonTransactionRepository()))->getLastTen());
    }
}
