<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    const TYPE_CREDIT = 'credit';

    const TYPE_DEBIT = 'debit';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'transactions';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount', 'user_id', 'type'
    ];
}
