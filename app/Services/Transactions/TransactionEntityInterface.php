<?php


namespace App\Services\t;


/**
 * Interface TransactionEntityInterface
 * @package App\Services\t
 */
interface TransactionEntityInterface
{

    /**
     * @return int
     */
    public function getId(): int ;

    /**
     * @return int
     */
    public function getUserId(): int ;

    /**
     * @return string
     */
    public function getType(): string ;

    /**
     * @return float
     */
    public function getAmount(): float ;
}
