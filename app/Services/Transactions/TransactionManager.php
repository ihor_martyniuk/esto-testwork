<?php
namespace App\Services\t;

use App\Exceptions\TransactionNotCreatedException;
use App\Repositories\Transactions\TransactionRepositoryInterface;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;

/**
 * Class TransactionManager
 * @package App\Services\t
 */
class TransactionManager implements TransactionManagerInterface
{
    /**
     * @var TransactionRepositoryInterface
     */
    protected TransactionRepositoryInterface $repository;

    /**
     * TransactionManager constructor.
     * @param TransactionRepositoryInterface $repository
     */
    public function __construct(TransactionRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $type
     * @param float $amount
     * @return TransactionEntityInterface
     * @throws AuthenticationException
     * @throws TransactionNotCreatedException
     * @throws \App\Exceptions\TransactionInitDataException
     */
    public function add(string $type, float $amount): TransactionEntityInterface
    {
        if (!Auth::check()) {
            throw new AuthenticationException('Only logged in users can create transactions!');
        }

        $data = [
            'type' => $type,
            'amount' => $amount,
            'user_id' => Auth::id(),
        ];

        if ($transaction = $this->repository->create($data)) {
            return TransactionEntity::init($transaction);
        } else {
            //TODO: need more validation acts for fully exception handling
            throw new TransactionNotCreatedException();
        }
    }

    /**
     * @return array
     */
    public function getLastTen(): array
    {
        return $this->repository->getLastTenUsersTransactions();
    }
}
