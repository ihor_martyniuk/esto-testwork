<?php


namespace App\Services\t;


use App\Exceptions\TransactionInitDataException;

/**
 * Class TransactionEntity
 * @package App\Services\t
 */
class TransactionEntity implements TransactionEntityInterface
{
    /**
     * @var int
     */
    protected int $id;
    /**
     * @var float
     */
    protected float $amount ;
    /**
     * @var int
     */
    protected int $userId ;
    /**
     * @var string
     */
    protected string $type ;

    /**
     * @param array $data
     * @return static
     * @throws TransactionInitDataException
     */
    public static function init(array $data): self
    {
        $entity = new static();

        /**
         * simple check for init data
         */
        if (
            !array_key_exists('amount', $data)
            || !array_key_exists('user_id', $data)
            || !array_key_exists('type', $data)
            || !array_key_exists('id', $data)
        ) {
            throw new TransactionInitDataException();
        }

        $entity->amount = $data['amount'];
        $entity->userId = $data['user_id'];
        $entity->type = $data['type'];
        $entity->id = $data['id'];

        return $entity;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
