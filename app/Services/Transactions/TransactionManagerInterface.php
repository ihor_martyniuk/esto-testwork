<?php


namespace App\Services\t;


/**
 * Interface TransactionManagerInterface
 * @package App\Services\t
 */
interface TransactionManagerInterface
{
    /**
     * @param string $type
     * @param float $amount
     * @return TransactionEntityInterface
     */
    public function add(string $type, float $amount): TransactionEntityInterface ;

    /**
     * @return array
     */
    public function getLastTen(): array ;
}
