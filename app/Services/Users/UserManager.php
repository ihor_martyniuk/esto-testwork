<?php


namespace App\Services\Users;


use App\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserManager
 * @package App\Services\Users
 */
class UserManager implements UserManagerInterface
{

    public function create(string $name, string $email, string $pass): bool
    {
        if (!Auth::check()) {
            throw new AuthenticationException('Only logged in users can create transactions!');
        }

        if (Auth::user()->permissions != User::ADMIN_TYPE) {
            throw new AuthorizationException('Only admin can create users!');
        }

        $model = new User();
        $model->name = $name;
        $model->email = $email;
        $model->password = $pass;
        $model->permissions = User::DEFAULT_TYPE;

        return $model->save();
    }
}
