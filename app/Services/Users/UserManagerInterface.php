<?php


namespace App\Services\Users;


/**
 * Interface UserManagerInterface
 * @package App\Services\Users
 */
interface UserManagerInterface
{

    /**
     * @param string $name
     * @param string $email
     * @param string $pass
     * @return bool
     */
    public function create(string $name, string $email, string $pass): bool ;
}
